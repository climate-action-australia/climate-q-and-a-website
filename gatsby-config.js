module.exports = {
  siteMetadata: {
    siteName: `Using Typescript Example`,
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/content/`,
      },
    },
    {
      resolve: "gatsby-transformer-remark",
      options: {
        // plugins: ["remark-include"]
      }
    },
    {
      resolve: 'gatsby-plugin-gtag',
      options: {
        // trackingId: 'UA-135060300-1',
        head: false, // Puts tracking script in the head instead of the body
        anonymize: true, // enable ip anonymization
      },
    },
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-typescript',
    // {
    //   resolve: `gatsby-plugin-typography`,
    //   options: {
    //     pathToConfigModule: `src/utils/typography.js`,
    //     omitGoogleFont: true,
    //   },
    // },
  ],
}
