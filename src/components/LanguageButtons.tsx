import React from 'react';

import './languageButtons.css'

interface Props {
    currentLanguage: string
}

export function LanguageButtons(props: Props) {
    const { currentLanguage } = props

    return (
        <div className="languageButtons">
            { currentLanguage !== "zh" && <a href="/zh">中文</a>}
            { currentLanguage !== "en" && <a href="/">Read in English</a>}
        </div>
    )
}