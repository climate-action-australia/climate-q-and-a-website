import React from "react";

interface Props {
  cards: {
    title: string
    title2?: string
    title3?: string
    
    titleNegativeMargin?: string // to try to make longer names fit into one line

    text?: string
    imageUrl: string
    buttons: {
      text: string
      url: string
    }[]

    facebookUrl?: string

  }[]

  imageHeight?: string
}

function cardClassNames(props: Props) {
  if (props.cards.length <= 2) {
    return "d-flex col-md-6"
  } else if (props.cards.length == 3) {
    return "col-sm-12 col-md-6 col-lg-4 action-box-list__item"
  } else if (props.cards.length == 4) {
    return "col-sm-12 col-md-6 col-lg-3 action-box-list__item"
  } else {
    return "col-sm-12 col-md-6 col-lg-4 action-box-list__item"
  }
}

export function CardContainer2(props: Props) {

  return (
    <div>
      <div className="container">
        <div className="row ">
          {props.cards.map(card => (
            <div key={card.title} className={cardClassNames(props)}>
              <div className="d-flex-inner">
                <div className="action-box action-box--green-mid">
                  <div className="action-box__inner">
                    <a href={card.buttons[0].url}>
                      <div  className="action-box__media lazyloaded"
                            style={{ backgroundImage: `url(${card.imageUrl})`, minHeight: props.imageHeight }}>
                      </div>
                    </a>

                    <div className="action-box__content">
                      <h2 style={{ marginLeft: card.titleNegativeMargin, marginRight: card.titleNegativeMargin}} className="action-box__content-title">
                        {card.title}
                        {card.title2 && <><br />{card.title2} </>}
                        {card.title3 && <><br />{card.title3} </>}
                      </h2>
                      <div className="action-box__content-content">
                        <p>{card.text || ''}</p>
                      </div>
                      <div className="action-box__content-action" style={{ display: "flex", marginTop: "unset" }}>
                        <div style={{ display: "flex", flexDirection: "column" }}>
                          {card.buttons.map(button => (
                            <a key={button.url} href={button.url} className="btn btn-primary" style={{marginTop: "0.5em"}}>
                              {button.text}
                            </a>
                          ))}
                        </div>
                        { card.facebookUrl &&
                          <a
                            className="person-contact__social-item"
                            href={card.facebookUrl}
                            style={{color: "white", alignSelf: "center", paddingLeft: "1em"}}
                          >
                            <i className="fa fa-2x fa-facebook-official" style={{fontSize: "3em"}}>
                              <span className="visually-hidden">Twitter</span></i>
                          </a>
                        }
                      </div>
                    </div>
                  </div>

                  <div className="action-box__border"></div>
                </div>

              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}