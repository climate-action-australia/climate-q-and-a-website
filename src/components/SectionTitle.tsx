import React from "react";

interface Props {
    title: string
}


export function SectionTitle(props: Props) {

    return (
        <div>
            <div className="page-section">
                <div className="page-section__heading ">
                    <div className="page-section__heading-content">
                        <div className="container">
                            <h2 className="page-section__title">{props.title}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}