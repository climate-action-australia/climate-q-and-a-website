const path = require("path")

exports.createPages = ({ actions, graphql }) => {

    const { createPage } = actions

    return new Promise(resolve => {

        // createPage({
        //     path: "/zh",
        //     component: path.resolve("src/pages/index.tsx"),
        //     context: {
        //         lang: "zh",
        //     }
        // })

        resolve()
    })



    const blogPostTemplate = path.resolve(`src/pages/FAQ.tsx`)

    return graphql(`
    {
      allMarkdownRemark(
        filter: { fileAbsolutePath: {regex: "/FAQ/"} }
        sort: { order: DESC, fields: [frontmatter___path] }
        limit: 1000
      ) {
        edges {
          node {
            id
            fileAbsolutePath
            frontmatter {
                path
                title
            }
          }
        }
      }
    }`).then(result => {
        if (result.errors) {
            return Promise.reject(result.errors)
        }

        result.data.allMarkdownRemark.edges.forEach(({ node }) => {
            createPage({
                path: node.frontmatter.path,
                component: blogPostTemplate,
                context: {},
            })
        })
    })
}