# Thank You to

* Authors of all the Climate Change resources and websites that have been cited and/or read.
* Gatsby, React, TypeScript, Visual Studio Code, and likely many other website development tools
* Zeit for now.sh hosting
* Gatsby Starter code: Fabien Bernard: fabien0102@gmail.com 
* Countless other living beings that have contributed indirectly