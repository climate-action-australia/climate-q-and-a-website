---
title: What can I do to support more action on climate change?
path: "/500-what-can-I-do-to-support-more-action"
---

There are countless ways that anyone can have many positive impacts on protecting our environment.

For example:

- Have conversation with your friends and family about the news & issues that you care about. [Here is a 4 minute video with some tips](https://ourclimateourfuture.org/video/secret-talking-climate-change/) and [here is an article](https://medium.com/s/story/how-to-have-a-useful-conversation-about-climate-change-in-11-steps-d4bbd4135e35).
- Write postal letters to your politicians
- Support public demonstrations such as the Global Climate Strikes
- Change your bank & superannuation to the most ethical investment option
- Get involved in political parties to support those that treat climate change as a serious priority or to push those that don't to improve their policies
- Support NGOs that are taking action to protect us from climate change
- Work on climate action as part of your career - [80,000 Hours has some advice](https://80000hours.org/problem-profiles/climate-change/)
- Follow, like and share useful & persuasive content on social media
- Buy less products with a high environmental impact
- Eat less meat and support foods that have a lower environmental impact
- Buy electricity from a reputable retailer that offers a "100% Renewables" option
- Where feasible reduce electricity, car and airplane use