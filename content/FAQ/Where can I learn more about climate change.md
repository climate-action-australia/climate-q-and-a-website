---
title: Where can I learn more about climate change?
path: "/900-where-can-I-learn-more-about-climate-change"
---

You could look through any of these websites:

- [Future of Life Institute - Op-ed: Climate Change Is the Most Urgent Existential Risk](https://futureoflife.org/2016/07/22/climate-change-is-the-most-urgent-existential-risk/)
- [NASA](https://climate.nasa.gov/)
- [The Royal Society](https://royalsociety.org/topics-policy/projects/climate-change-evidence-causes/)
- [The American Chemical Society](https://www.acs.org/content/acs/en/climatescience.html)
- [The Australian Academy of Science](https://www.science.org.au/education/immunisation-climate-change-genetic-modification/science-climate-change)
- [IPCC Reports](https://www.ipcc.ch/reports/)
- [List of Resources from the Effective Altruism community](https://docs.google.com/document/d/1QoAdW2la3fKM9WbzST4Vrm5WgV9P9dhe6bXKw-J1Vqw/edit)

Videos and films include:

- "Climate Change: The Facts" with Sir David Attenborough. Available for viewing [via ABC iView in Australia](https://iview.abc.net.au/show/climate-change-the-facts), [via the BBC](https://www.bbc.co.uk/programmes/p076w7g5), and likely other sources.`
- ["It’s time to act on climate change" from the OECD](https://www.youtube.com/watch?v=Wkr501nu3NA)
- ["Causes and Effects of Climate Change | National Geographic"](https://www.youtube.com/watch?v=G4H1N_yXBiA)