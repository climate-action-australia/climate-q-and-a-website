---
title: Does renewable energy contribute to Big Government?
path: "/013-Does-renewable-energy-contribute-to-big-government"
---


Renewable energy policy is sometimes criticised as a "Big Government" over-reach. However it should be noted that renewables give us the potential to create a much more decentralised energy system. Currently electricity is generated mainly by large corporations operating large expensive heavily-regulated power stations. Technologies like wind and solar are enabling smaller-scale players to enter the market.

It will even be possible to operate independently of the government-regulated national energy grid and rely, for example, on one's own solar panels and batteries.