---
title: Can renewable energy meet our electricity needs?
path: "/011-Can-renewable-energy-meet-our-electricity-needs"
---


It is often said that wind and solar are unable to do the job because they don't work when the wind isn't blowing or the sun isn't shining. That is a half-truth as electricity can be stored using emerging technologies like pumped hydro, batteries and hydrogen. Australia's CSIRO has found that there is no technical impediment to 100% renewable electricity and another CSIRO report has found that wind and solar are the cheapest way to build more generation even with 6 hours of storage.


#### References


* [CSIRO says Australia can get to 100 per cent renewable energy](https://reneweconomy.com.au/csiro-says-australia-can-get-100-per-cent-renewable-energy-86624/)
* [AEMO and CSIRO find renewables cheapest](https://arena.gov.au/blog/gencost2018/)