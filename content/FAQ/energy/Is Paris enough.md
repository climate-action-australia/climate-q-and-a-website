---
title: Are the Paris targets enough?
path: "/012-Are-the-Paris-targets-enough"
---

TODO

# http://climatechangeauthority.gov.au/sites/prod.climatechangeauthority.gov.au/files/Final-report-Australias-future-emissions-reduction-targets.pdf


If all humans were entitled to release the same greenhouse emissions by 2050, the average would be around 2 tonnes of CO₂ per person in 2050. In 2018, the average Australian was responsible for 21.5 tonnes.

https://theconversation.com/australia-is-counting-on-cooking-the-books-to-meet-its-climate-targets-110768