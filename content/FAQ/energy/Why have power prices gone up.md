---
title: Why have power prices gone up? Is renewable energy to blame?
path: "/010-why-have-power-prices-gone-up--Is-renewable-energy-to-blame"
---


Power prices have gone up for many reasons:

- Price gouging by the electricity companies, as enabled by privatisation. Even the Morrison Coalition government is indirectly admitting this by publicly pressuring companies to reduce prices & even considered price controls - because they can.
- Lack of new generation & storage due to the private sector being unwilling to make investments given the unstable politics around energy policy.
- A huge increase in gas prices after new LNG facilities enabled Australian gas to be exported to Asia where the market price is much higher than what Australia had before.
- Renewable energy incentives have played a role but it should be remembered that this is an investment in our energy future and reduces the far greater costs of fossil-fuels (worser bushfires, drought, storm damage, sea-level rise & other damage from climate change, air pollution). Australia’s contribution is critical in creating momentum for larger countries to take action too.


## References

[ The stunning wind, solar and battery costs the Coalition refuses to accept - Renew Economy](https://reneweconomy.com.au/the-stunning-wind-solar-and-battery-costs-the-coalition-refuses-to-accept-31985/)  
Unsubsidised wind and solar are now substantially cheaper than coal, even with the added cost of storage that makes these “intermittent” sources fully “dispatchable”.


[The inconvenient truths about South Australia’s renewable success - Renew Economy](https://reneweconomy.com.au/the-inconvenient-truths-about-south-australias-renewable-success-86561/)  
Over the last decade average wholesale prices have declined in South Australia, the state with the most renewables, but have gone up in the states with the least. South Australia has always had high prices, which is why the state generator started considering wind energy almost 50 years ago. 

[Expert’s advice to aluminium giant: Wind and solar power beat coal on price - Energy Matters](https://www.energymatters.com.au/renewable-news/wind-solar-energy-beats-coal-says-expert/)  
Wind and solar energy beat coal-fired plants on efficiency and price, even for heavy industry like steel