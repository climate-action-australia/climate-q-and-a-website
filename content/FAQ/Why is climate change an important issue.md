---
title: Why is climate change an important issue?
path: "/0-why-is-climate-change-important"
---


There is an overwhelming consensus amongst the world's [climate scientists](https://theconversation.com/groupthink-is-it-a-valid-argument-against-climate-science-123050) and reputable scientific organisations like NASA, the CSIRO and the Royal Society, that humanity is contributing to significant changes in the Earth's climate. Humans have increased CO2 to a level that is unprecendented in the last 800,000 years and the current speed of warming is more than ten times more rapid than the fastest natural increases (at the ends of previous ice ages).[1]

![chart](https://royalsociety.org/-/media/Royal_Society_Content/policy/projects/climate-evidence-causes/fig3-large.jpg)

There are two kinds of problems:

## 1. Direct harms from Climate Change

For example, Australia's CSIRO [reports that effects are expected to include:](https://www.csiro.au/en/Research/OandA/Areas/Assessing-our-climate/State-of-the-Climate-2018/Future-climate)

- More high fire weather danger days and a longer fire season for southern and eastern Australia.
- More drought across many regions of southern Australia.
- More short-duration extreme rainfall events.
- Increasing sea level rise threatening coastal communities.
- Further warming and acidification of the oceans around Australia, leading to more severe bleaching of the Great Barrier Reef, and potentially the loss of many types of coral.
- Fewer tropical cyclones, but a greater proportion of high-intensity storms, with ongoing large variations from year to year.

In 2019 Australia is already seeing more extreme [bushfires](https://theconversation.com/its-only-october-so-whats-with-all-these-bushfires-new-research-explains-it-124091), a very severe drought, plus extremely damaging flooding events like that in Queensland.

Countries with poorer people, who have done the least to cause global warming, stand to suffer the most.

There will always be some uncertainty in very complex climate change predictions. However that does not mean we shouldn't take vigorous action to protect future generations from the risk of climate-related disaster, which may include a mass-extinction.

## 2. Catastrophic/Existential Risks

Past natural, and slower, climate changes have had major effects on the Earth, leading to the extinction of many species, massive rises in sea levels, the formation of deserts and more. Furthermore, modern human civilisation relies on many complex interacting systems - such as the distribution of food, and the peaceful coexistance of sovereign states. If these systems are severely undermined, human civilisation may collapse beyond repair. Climate Change is one of the factors that is contributing to this risk. For example, crop failures from drought, water shortages, and sea level rise could lead to conflict that spirals into nuclear war. Current conflicts like the Civil War in Syria are directly linked to drought.

While there is much more uncertainty about these types of risk compared to the direct harms, their severity means they should arguably still be taken very seriously.


#### References

* [1]: [The Royal Society: Climate is always changing. Why is climate change of concern now? ](https://royalsociety.org/topics-policy/projects/climate-change-evidence-causes/question-6/)
* [Future of Life Institute Podcast: The Climate Crisis as an Existential Threat with Simon Beard and Haydn Belfield](https://futureoflife.org/2019/08/01/the-climate-crisis-as-an-existential-threat-with-simon-beard-and-haydn-belfield/)
* [NASA: Climate Change and Global Warming](https://climate.nasa.gov/)
* [CSIRO - State of the Climate 2018](https://www.csiro.au/en/Research/OandA/Areas/Assessing-our-climate/State-of-the-Climate-2018/Future-climate)