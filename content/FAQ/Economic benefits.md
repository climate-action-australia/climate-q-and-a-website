---
title: Are there economic benefits from taking action on climate change?
path: "/003-are-there-economic-benefits"
---

TODO

In addition to avoiding the huge costs & risks of inaction, most action on climate change will actually deliver economic benefits, especially jobs. This would not only be in the renewable energy industry, but also in other industries as they modernise to reduce their environmental impacts. Wind turbines don't grow by themselves like trees do.
