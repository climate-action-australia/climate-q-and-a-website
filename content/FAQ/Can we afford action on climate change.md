---
title: Can we afford action on climate change?
path: "/002-can-we-afford-action-on-climate-change"
---

Vigorous action is not cheap or easy but spending the necessary resources is eminently feasible if we have the political will. For example, during WW1 and WW2 we recognised that losing was not an option and vast resources were spent on the wars. Our countries did not go bankrupt and there was actually a significant economic boom after WW2, even despite the enormous death, injury and destruction of the war. Action on climate change is nowhere as difficult or costly as a major war.

[Australia's Treasury has reported that](https://treasury.gov.au/publication/p2011-sglp-overview/costs-of-inaction/) it is not investing in countering climate change that will very likely cost more in the long-term than taking action. The costs include at the very least more severe natural disasters, more severe droughts, and loss of ecosystems like the Great Barrier Reef which supports 60,000 tourism jobs.

There are many ways to raise revenue by for example eliminating special arrangements that, for example, [enable billions in profits from our offshore gas resources to be extracted with no royalties and very little in income tax](http://theconversation.com/senate-inquiry-told-zero-tax-or-royalties-paid-on-australias-biggest-new-gas-projects-77479).