---
title: Doesn't climate action block developing countries from modernising?
path: "/014-developing-countries"
---

TODO - renewables, going straight into the 21st century

### Pollution

https://www.abc.net.au/news/2018-12-07/pollution-claimed-1.24-million-lives-in-india-in-2017/10595948
India's toxic air claimed 1.24 million lives in 2017, or 12.5 per cent of total deaths recorded that year, according to a study published in Lancet Planetary Health.
The World Health Organisation says India is home to the world's 14 most polluted cities
Average life expectancy in India in 2017 would have been higher by 1.7 years if air quality was at healthy levels, the report said. That isn't as gloomy as some other recent studies. For example, a University of Chicago report released last month said prolonged exposure to pollution reduces the life expectancy of an Indian citizen by over 4 years.


Two-thirds of all air pollution deaths occur in Asia
Judging from World Health Organization figures released on Wednesday, covering 4,300 cities across 108 countries, the commuters have the right idea. Of an estimated 7 million deaths worldwide per year from air pollution, just over two-thirds take place in Asia, which is home to slightly less than 60% of the global population.
The WHO says 90% of the worldwide death toll comes from less wealthy countries, and that "98% of cities in low- and middle-income countries with more than 100,000 inhabitants do not meet WHO air quality guidelines." Though that number drops to 56% in high-income countries, the WHO estimates that 9 of 10 people worldwide breathe polluted air at one time or another.
https://asia.nikkei.com/Economy/Two-thirds-of-all-air-pollution-deaths-occur-in-Asia


The impact of air pollution on deaths, disease burden, and life expectancy across the states of India: the Global Burden of Disease Study 2017
Published:December 05, 2018
one out of every eight deaths in India in 2017 could be attributed to air pollution.
The 1·38 billion people in India in 2017 made up 18·1% of the global 7·64 billion population, but India had 38·7 million (26·2%) of the global 147·4 million DALYs attributable to air pollution in 2017.42one out of every eight deaths in India in 2017 could be attributed to air pollution.
https://www.thelancet.com/journals/lanplh/article/PIIS2542-5196(18)30261-4/fulltext
